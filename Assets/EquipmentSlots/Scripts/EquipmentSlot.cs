﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class EquipmentSlot : MonoBehaviour
{
    public Item currentItem = null;
    public Action OnEquip;
    public ItemSlot slot;

    [SerializeField] GameObject slotText;
    [SerializeField] UiEquipment equipment;
    [SerializeField] Image icon;

    bool canDrop = false;
    bool didEquip = false;
    bool dropDone = false;

    Item item = null;

    public void OnPointerEnter()
    {
        if (equipment.dragController.currentDragItem != null)
        {
            item = equipment.dragController.currentDragItem.GetComponent<DragInventoryItem>().item;
            canDrop = true;
        }
    }

    public void OnPointerExit()
    {
        if (Application.isEditor || dropDone || Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
        {
            if (!didEquip)
            {
                item = null;
                didEquip = false;
            }

            canDrop = false;
        }

        dropDone = false;
    }

    /// <summary>
    /// Pass null to UnEquip Item
    /// </summary>
    /// <param name="item"></param>
    public void Equip(Item item)
    {
        if (item != null && item.slot == slot)
        {
            currentItem = item;
            icon.sprite = currentItem.icon;
            slotText.gameObject.SetActive(false);
            PlayerPrefs.SetString(gameObject.name, currentItem.item_name);

            OnEquip?.Invoke();
        }
        else if (item == null)
        {
            currentItem = item;
            icon.sprite = null;
            slotText.gameObject.SetActive(true);
            PlayerPrefs.DeleteKey(gameObject.name);
        }
    }

    public void LoadSaved(ItemConfig config)
    {
        if (PlayerPrefs.HasKey(gameObject.name))
        {
            Item savedItem = config.items.Find(e => e.item_name.Equals(PlayerPrefs.GetString(gameObject.name)));
            Equip(savedItem);
        }
    }

    void OnDrop()
    {
        if (item != null && item.slot == slot)
        {
            Equip(item);
            didEquip = true;
        }

        dropDone = true;

        OnPointerExit();
    }

    void Start()
    {
    }

    void Update()
    {
        if (canDrop)
        {
            if (Input.GetMouseButtonUp(0) || Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                OnDrop();
            }
        }
    }
}
