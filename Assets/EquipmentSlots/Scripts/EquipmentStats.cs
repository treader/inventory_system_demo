﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentStats : MonoBehaviour
{
    [SerializeField] StatConfig statConfig;

    [SerializeField] Text textDamage;
    [SerializeField] Text textDefence;
    [SerializeField] Text textStr;
    [SerializeField] Text textAgi;
    [SerializeField] Text textInt;

    [SerializeField] Text textHp;
    [SerializeField] Text textMana;
    [SerializeField] Text textDodge;
    [SerializeField] Text textCritical;

    struct BaseStats
    {
        public float damage;
        public float defence;
        public float str;
        public float agi;
        public float intel;
    }

    Item[] equippedtems;
    BaseStats baseStats;
    BaseStats compareStats;

    public void UpdateStats(Item[] equippedtems)
    {
        this.equippedtems = equippedtems;

        ResetStats();

        if (equippedtems != null)
        {
            for (int i = 0; i < equippedtems.Length; i++)
            {
                baseStats.damage += equippedtems[i].damage;
                baseStats.defence += equippedtems[i].defence;

                baseStats.str += equippedtems[i].strength;
                baseStats.agi += equippedtems[i].agility;
                baseStats.intel += equippedtems[i].intel;
            }
        }

        SetBaseStatText();
        SetModifierText();
    }

    public void ItemSelected(Item selectedItem)
    {
        ResetCompareStats();
        Item itemToReplace = GetItemToReplace(selectedItem);

        if (itemToReplace != null)
        {
            Item[] simulatedEquppedItems = GetSimulateEquippedItems(itemToReplace, selectedItem);

            UpdateCompareStats(simulatedEquppedItems);
            UpdateTextWithCompare();
        }
    }

    void SetBaseStatText()
    {
        textDamage.text = StatPrefix.DAMAGE_PREFIX + baseStats.damage.ToString();
        textDefence.text = StatPrefix.DEFENCE_PREFIX + baseStats.defence.ToString();

        textStr.text = StatPrefix.STR_PREFIX + baseStats.str.ToString();
        textAgi.text = StatPrefix.AGI_PREFIX + baseStats.agi.ToString();
        textInt.text = StatPrefix.INT_PREFIX + baseStats.intel.ToString();
    }

    void SetModifierText()
    {
        textHp.text = StatPrefix.HP_PREFIX + statConfig.GetValueByType(ModifierType.HP, baseStats.str);
        textMana.text = StatPrefix.MANA_PREFIX + statConfig.GetValueByType(ModifierType.Mana, baseStats.intel).ToString();
        textDodge.text = StatPrefix.DODGE_PREFIX + statConfig.GetValueByType(ModifierType.Dodge, baseStats.agi) + "%";
        textCritical.text = StatPrefix.CRITICAL_PREFIX + statConfig.GetValueByType(ModifierType.CriticalHit, baseStats.agi) + "%";
    }

    void ResetStats()
    {
        baseStats.damage = 0;
        baseStats.defence = 0;
        baseStats.str = 0;
        baseStats.agi = 0;
        baseStats.intel = 0;
    }

    void ResetCompareStats()
    {
        compareStats.damage = 0;
        compareStats.defence = 0;
        compareStats.str = 0;
        compareStats.agi = 0;
        compareStats.intel = 0;
    }

    Item GetWeakestWeapon(Item[] weapons)
    {
        Item currentWeakestWeapon = null;

        for (int i = 0; i < weapons.Length; i++)
        {
            if (currentWeakestWeapon == null)
            {
                currentWeakestWeapon = weapons[i];
            }
            else
            {
                if (weapons[i].damage < currentWeakestWeapon.damage)
                {
                    currentWeakestWeapon = weapons[i];
                }
            }
        }

        return currentWeakestWeapon;
    }

    Item GetItemToReplace(Item selectedItem)
    {
        Item itemToReplace = null;
        if (selectedItem.slot == ItemSlot.Weapon)
        {
            Item[] weapons = Array.FindAll(equippedtems, e => e.slot == ItemSlot.Weapon);
            if (weapons.Length > 1)
            {
                itemToReplace = GetWeakestWeapon(weapons);
            }
            else if(weapons.Length > 0)
            {
                itemToReplace = weapons[0];
            }
        }
        else
        {
            itemToReplace = Array.Find<Item>(equippedtems, e => e.slot == selectedItem.slot);
        }

        return itemToReplace;
    }

    Item[] GetSimulateEquippedItems(Item itemToReplace, Item selectedItem)
    {
        Item[] simulatedEquppedItems = new Item[equippedtems.Length];
        int replaceIndex = Array.FindIndex<Item>(equippedtems, e => e.item_name.Equals(itemToReplace.item_name));
        
        for (int i = 0; i < simulatedEquppedItems.Length; i++)
        {
            if (i == replaceIndex)
            {
                simulatedEquppedItems[i] = selectedItem;
            }
            else
            {
                simulatedEquppedItems[i] = equippedtems[i];
            }
        }

        return simulatedEquppedItems;
    }

    void UpdateCompareStats(Item[] items)
    {
        if (items != null)
        {
            for (int i = 0; i < items.Length; i++)
            {
                compareStats.damage += items[i].damage;
                compareStats.defence += items[i].defence;

                compareStats.str += items[i].strength;
                compareStats.agi += items[i].agility;
                compareStats.intel += items[i].intel;
            }
        }
    }

    void UpdateTextWithCompare()
    {
        SetBaseStatText();

        textDamage.text += GetDiffValue(compareStats.damage, baseStats.damage);
        textDefence.text += GetDiffValue(compareStats.defence, baseStats.defence);

        textStr.text += GetDiffValue(compareStats.str, baseStats.str);
        textAgi.text += GetDiffValue(compareStats.agi, baseStats.agi);
        textInt.text += GetDiffValue(compareStats.intel, baseStats.intel);
    }

    string GetDiffValue(float compare, float baseValue)
    {
        float diff = compare - baseValue;
        string value = string.Empty;

        if (diff < 0)
        {
            value = diff.ToString();
        }
        else if (diff > 0)
        {
            value = "+" + diff;
        }


        return value;
    }
}
