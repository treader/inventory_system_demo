﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

public class UiEquipment : MonoBehaviour
{
    public DragController dragController;
    public EquipmentStats equipmentStats;

    [SerializeField] EquipmentSlot[] equipmentSlots;

    public void ItemEquipSelected(Item selectedItem)
    {
        EquipmentSlot slot = Array.Find<EquipmentSlot>(equipmentSlots, e => e.slot.Equals(selectedItem.slot));

        if (slot != null)
        {
            if (slot.currentItem == null || !slot.currentItem.item_name.Equals(selectedItem.item_name))
            {
                slot.Equip(selectedItem);
            }
            else
            {
                slot.Equip(null);
            }
        }
    }

    public void LoadSaved(ItemConfig itemConfig)
    {
        for (int i = 0; i < equipmentSlots.Length; i++)
        {
            equipmentSlots[i].LoadSaved(itemConfig);
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        equipmentStats.UpdateStats(null);
        SubscribeToEquipSlots();
    }

    void SubscribeToEquipSlots()
    {
        for (int i = 0; i < equipmentSlots.Length; i++)
        {
            equipmentSlots[i].OnEquip += HandleSlotEquipped;
        }
    }

    void HandleSlotEquipped()
    {
        EquipmentSlot[] equippedSlots = Array.FindAll<EquipmentSlot>(equipmentSlots, e => e.currentItem != null && !string.IsNullOrEmpty(e.currentItem.item_name));
        Item[] items = new Item[equippedSlots.Length];

        for (int i = 0; i < items.Length; i++)
        {
            items[i] = equippedSlots[i].currentItem;
        }

        equipmentStats.UpdateStats(items);
    }
}
