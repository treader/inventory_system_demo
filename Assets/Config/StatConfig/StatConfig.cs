﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class StatConfig : ScriptableObject
{
    [Serializable]
    public struct StatData
    {
        public ModifierType modifierType;
        public float modiferValue;
    }

    [SerializeField] List<StatData> statData = new List<StatData>();

    public float GetValueByType(ModifierType type, float value)
    {
        float modifier = statData.Find(e => e.modifierType == type).modiferValue;

        return value * modifier;
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Data/Stat config")]
    public static void Create()
    {
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/StatConfig.asset");

        StatConfig asset = CreateInstance<StatConfig>();
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif
}

public enum ModifierType
{
    Dodge,
    CriticalHit,
    Mana,
    HP
}