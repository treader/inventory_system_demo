﻿using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ItemColorConfig : ScriptableObject
{
    [Serializable]
    public struct ColorData
    {
        public ItemClass itemClass;
        public Color classColor;
    }

    [SerializeField] List<ColorData> colorData = new List<ColorData>();

    public Color GetValueByClass(ItemClass itemClass)
    {
        return colorData.Find(e => e.itemClass == itemClass).classColor;
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Data/Class color config")]
    public static void Create()
    {
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/ClassColorConfig.asset");

        ItemColorConfig asset = CreateInstance<ItemColorConfig>();
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif
}
