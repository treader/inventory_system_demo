﻿using UnityEngine;
using UnityEngine.UI;

public class DragInventoryItem : MonoBehaviour
{
    public Item item;
    [SerializeField] Image icon;

    void Start()
    {
        icon.sprite = item.icon;
    }
}
