﻿using System;
using UnityEngine;

public class UiInventorySystem : MonoBehaviour
{
    [SerializeField] UiInventory inventory;
    [SerializeField] UiEquipment equipment;

    // Start is called before the first frame update
    void Start()
    {
        inventory.OnItemSelected += HandleItemSelected;
        inventory.OnItemEquip += HandleEquipUnEquipClicked;

        equipment.LoadSaved(inventory.itemConfig);
    }

    void HandleEquipUnEquipClicked(Item item)
    {
        equipment.ItemEquipSelected(item);
    }

    void HandleItemSelected(Item item)
    {
        equipment.equipmentStats.ItemSelected(item);
    }
}
