﻿public class StatPrefix
{
    public const string DAMAGE_PREFIX = "Damage: ";
    public const string DEFENCE_PREFIX = "Defence: ";
    public const string STR_PREFIX = "STR: ";
    public const string INT_PREFIX = "INT: ";
    public const string AGI_PREFIX = "AGI: ";

    public const string HP_PREFIX = "HP: ";
    public const string MANA_PREFIX = "Mana: ";
    public const string DODGE_PREFIX = "Dodge: ";
    public const string CRITICAL_PREFIX = "Crtical: ";
}
