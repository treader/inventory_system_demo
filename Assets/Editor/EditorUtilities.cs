﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorUtilities : MonoBehaviour
{
    [MenuItem("Tools/ClearData")]
    static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
