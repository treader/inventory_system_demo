﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    public Action<Item> OnItemClicked;

    [SerializeField] Image icon;
    [SerializeField] Image classBg;

    [SerializeField] DragController dragController;
    [SerializeField] DragInventoryItem dragInventoryItemTemplate;
    [SerializeField] ItemColorConfig itemColorConfig;

    Item thisItem;

    public void InitItem(Item item)
    {
        thisItem = item;
        icon.sprite = item.icon;
        classBg.color = itemColorConfig.GetValueByClass(item.item_class);
        gameObject.SetActive(true);
    }

    public void ItemClick()
    {
        OnItemClicked?.Invoke(thisItem);
    }

    public void BeginDrag()
    {
        GameObject dragItem = GameObject.Instantiate(dragInventoryItemTemplate.gameObject);
        dragItem.GetComponent<DragInventoryItem>().item = thisItem;
        dragController.InitDrag(dragItem);
    }
}
