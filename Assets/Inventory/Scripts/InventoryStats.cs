﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryStats : MonoBehaviour
{
    [SerializeField] Image icon;
    [SerializeField] Text textName;
    [SerializeField] Text textDescription;
    [SerializeField] Text textClass;
    [SerializeField] Text textDamage;
    [SerializeField] Text textDefence;
    [SerializeField] Text textSTR;
    [SerializeField] Text textINT;
    [SerializeField] Text textAGI;

    public void UpdateStats(Item item)
    {
        icon.sprite = item.icon;

        textName.text = item.item_name;
        textDescription.text = item.description;

        textClass.text = item.item_class.ToString();

        textDamage.text = StatPrefix.DAMAGE_PREFIX + item.damage;
        textDefence.text = StatPrefix.DEFENCE_PREFIX + item.defence;

        textSTR.text = StatPrefix.STR_PREFIX + item.strength;
        textINT.text = StatPrefix.INT_PREFIX + item.intel;
        textAGI.text = StatPrefix.AGI_PREFIX + item.agility;
    }
}
