﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UiInventory : MonoBehaviour
{
    enum SortType
    {
        Type = 1,
        Class = 2,
        Alphabetical = 3
    }

    public Action<Item> OnItemSelected;
    public Action<Item> OnItemEquip;

    public ItemConfig itemConfig;

    [SerializeField] InventoryItem itemTemplate;
    [SerializeField] GridLayoutGroup gridLayout;
    [SerializeField] InventoryStats inventoryStats;
    [SerializeField] Text currentSort;

    Item selectedItem = null;

    List<InventoryItem> inventoryItems;
    List<Item> populatedItems;

    int typeCounts = 0;
    int currentSortIndex = 0;

    public void EquipUnEquipClicked()
    {
        OnItemEquip?.Invoke(selectedItem);
    }

    public void DoSort()
    {
        SortType currentType;
        currentSortIndex++;

        if (currentSortIndex > typeCounts)
        {
            currentSortIndex = 1;
        }

        currentType = (SortType)currentSortIndex;
        currentSort.text = currentType.ToString();

        switch (currentType)
        {
            case SortType.Type:
                populatedItems.Sort(
                    delegate(Item item1, Item item2)
                    {
                        return item1.slot.CompareTo(item2.slot);
                    }
                    );
                break;
            case SortType.Class:
                populatedItems.Sort(
                    delegate (Item item1, Item item2)
                    {
                        return item1.item_class.CompareTo(item2.item_class);
                    }
                    );
                break;
            case SortType.Alphabetical:
                populatedItems.Sort(
                    delegate (Item item1, Item item2)
                    {
                        return item1.item_name.CompareTo(item2.item_name);
                    }
                    );
                break;
        }

        PopulateSortedItems();
    }

    // Start is called before the first frame update
    void Start()
    {
        inventoryItems = new List<InventoryItem>();
        populatedItems = new List<Item>();

        typeCounts = Enum.GetNames(typeof(SortType)).Length;

        PopulateInventoryItems();
        inventoryStats.UpdateStats(itemConfig.items[0]);
    }

    void PopulateInventoryItems()
    {
        for (int i = 0; i < itemConfig.items.Count; i++)
        {
            InstantiateItem(itemConfig.items[i]);
        }
    }

    void InstantiateItem(Item item)
    {
        InventoryItem inventoryItem = GameObject.Instantiate(itemTemplate.gameObject, gridLayout.transform).GetComponent<InventoryItem>();

        inventoryItems.Add(inventoryItem);
        populatedItems.Add(item);

        inventoryItem.InitItem(item);
        inventoryItem.OnItemClicked += HandleItemSelected;
    }

    void HandleItemSelected(Item selectedItem)
    {
        inventoryStats.UpdateStats(selectedItem);
        this.selectedItem = selectedItem;

        OnItemSelected?.Invoke(selectedItem);
    }

    void PopulateSortedItems()
    {
        for (int i = 0; i < populatedItems.Count; i++)
        {
            inventoryItems[i].InitItem(populatedItems[i]);
        }
    }
}
