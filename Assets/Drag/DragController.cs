﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragController : MonoBehaviour
{
    public GameObject currentDragItem;

    [SerializeField] Transform dragCanvas;

    bool canDrag = false;
    Vector2 mousePos = Vector2.zero;

    public void InitDrag(GameObject dragItem)
    {
        currentDragItem = dragItem;
        currentDragItem.transform.SetParent(dragCanvas);
        currentDragItem.transform.SetAsLastSibling();
        canDrag = true;
    }

    public void OnDrag()
    {
        currentDragItem.transform.position = Input.mousePosition;
    }

    public void OnDrop()
    {
        Destroy(currentDragItem);
        canDrag = false;
    }

    private void Update()
    {
        if (canDrag)
        {
            if (Input.GetMouseButton(0))
            {
                OnDrag();
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnDrop();
            }
        }
    }
}
