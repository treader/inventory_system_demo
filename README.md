# inventory_system_demo

Made using: Unity3D 2018.3.2f1

Platform: Android (Landscape)

How to use

-Clone from develop branch

-Start it in the inventory scene

Configurations:

Items can be changed in the ItemData Scriptable object

Item class color can be changed in the ClassColorConfig Scriptable object

Item stat modifers can be changed in the StatConfig Scriptable object